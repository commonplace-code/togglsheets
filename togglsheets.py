#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from typing import Dict, List

import colorlog
import logging
import requests
import sys
import xlsxwriter

from datetime import datetime, timedelta
from optparse import OptionParser
from pathlib import Path
from yaml_config_wrapper import Configuration

VERSION = "0.0.1"

log_handler = colorlog.StreamHandler()
log_handler.setFormatter(colorlog.ColoredFormatter('%(log_color)s%(levelname)s:%(name)s:%(message)s'))
log = colorlog.getLogger('togglesheets')
log.addHandler(log_handler)
log.setLevel(logging.INFO)

DEFAULT_CONFIG_PATH = './config.yml'
DEFAULT_DATE_FORMAT = '%d-%m-%Y'
DEFAULT_OUTPUT_FORMAT = '{project}_detailed_report_{since}_{until}.xlsx'


class Project:
    def __init__(self, **kwargs):
        if 'id' not in kwargs:
            log.error(f'Missing id for project: {kwargs}')

        if 'name' not in kwargs:
            log.error(f'Missing name for project: {kwargs}')

        for key, value in kwargs.items():
            self.__setattr__(key, value)

    def __str__(self) -> str:
        return f"{self.id}:{self.name}"


class Projects:
    def __init__(self):
        self._projects_by_id = {}

    def add(self, project: Project) -> bool:
        if project.id <= 0:
            log.error(f"Not a valid project id: {project}")
            return False

        if project.name == "":
            log.error(f"Not a valid project name: {project}")
            return False

        if project.id in self._projects_by_id:
            log.error(f"Project already added: {project}")
            return False

        self._projects_by_id[project.id] = project
        return True

    @property
    def projects(self) -> List:
        return list(self._projects_by_id.values())

    def project_by_id(self, project_id: int) -> (Project, None):
        if project_id not in self._projects_by_id:
            log.warning(f"Project not found: {project_id}")
            return None

        return self._projects_by_id[project_id]

    def project_by_name(self, project_name: str) -> (Project, None):
        for _, prj in self._projects_by_id.items():
            if prj.name == project_name:
                return prj

        log.warning(f"Project not found: {project_name}")
        return None


class Workspace:
    PROJECTS_URL = "https://api.track.toggl.com/api/v8/workspaces/{ws_id}/projects?actual_hours=true"

    def __init__(self, **kwargs):
        self._projects = Projects()

        if 'id' not in kwargs:
            log.error(f'Missing id for workspace: {kwargs}')

        if 'name' not in kwargs:
            log.error(f'Missing name for workspace: {kwargs}')

        for key, value in kwargs.items():
            self.__setattr__(key, value)

        self._retrieve_projects()

    def _retrieve_projects(self):
        resp = requests.get(Workspace.PROJECTS_URL.format(ws_id=self.id), auth=(self.api_token, 'api_token'))
        if resp.json() is not None:
            for entry in resp.json():
                self._projects.add(Project(**entry))

    @property
    def projects(self) -> Projects:
        return self._projects

    def project_by_id(self, project_id: int) -> (Project, None):
        return self._projects.project_by_id(project_id)

    def project_by_name(self, project_name: str) -> (Project, None):
        return self._projects.project_by_name(project_name)

    def __str__(self) -> str:
        return f"{self.id}:{self.name}"


class Workspaces:
    WORKSPACES_URL = "https://api.track.toggl.com/api/v8/workspaces"

    def __init__(self, api_token):
        self._api_token = api_token
        self._workspaces_by_id = {}

    @property
    def workspaces(self) -> List:
        return list(self._workspaces_by_id.values())

    def get_workspaces(self):
        resp = requests.get(Workspaces.WORKSPACES_URL, auth=(self._api_token, 'api_token'))
        for entry in resp.json():
            log.debug(entry)

            # there is an incorrect api_token in the workspace return payload?
            # overwrite with correct token
            entry['api_token'] = self._api_token
            self.add(Workspace(**entry))

    def add(self, workspace: Workspace) -> bool:
        if workspace.id <= 0:
            log.error(f"Not a valid workspace id: {workspace}")
            return False

        if workspace.name == "":
            log.error(f"Not a valid workspace name: {workspace}")
            return False

        if workspace.id in self._workspaces_by_id:
            log.error(f"Workspace already added: {workspace}")
            return False

        self._workspaces_by_id[workspace.id] = workspace
        return True

    def workspace_by_id(self, workspace_id: int) -> (Workspace, None):
        if workspace_id not in self._workspaces_by_id:
            log.warning(f"Workspace not found: {workspace_id}")
            return None

        return self._workspaces_by_id[workspace_id]

    def workspace_by_name(self, workspace_name: str) -> (Workspace, None):
        for _, ws in self._workspaces_by_id.items():
            if ws.name == workspace_name:
                return ws

        log.warning(f"Workspace not found: {workspace_name}")
        return None


class TimeEntry:
    def __init__(self, **kwargs):
        if 'id' not in kwargs:
            log.error(f'Missing id for time entry: {kwargs}')

        for key, value in kwargs.items():
            self.__setattr__(key, value)

        self.start = datetime.fromisoformat(self.start)
        self.start.replace(hour=0, minute=0, second=0)
        self.end = datetime.fromisoformat(self.end)
        self.end.replace(hour=0, minute=0, second=0)
        self.duration = timedelta(milliseconds=self.dur)
        self.duration_decimal = self._hours_decimal(self.duration)

    def _hours_decimal(self, duration: timedelta) -> float:
        minutes, _ = divmod(duration.seconds, 60)
        hour, minutes = divmod(minutes, 60)
        return hour + minutes / 60


class TimeSheet:
    TOGGL_REPORT_URL = "https://api.track.toggl.com/reports/api/v2/details?workspace_id={workspace_id}&since={since}&until={until}&project_ids={projects}&rounding={rounding}&order_field=date&order_desc=on&user_agent=togglsheets"

    def __init__(self, api_token: str, workspace: Workspace, project: Project, start_date: datetime,
                 end_date: datetime = datetime.now(), rounding=True, email=''):
        self.workspace = workspace
        self.project = project
        self.start_date = start_date
        self.end_date = end_date
        self.rounding = rounding
        self.email = email

        self._api_token = api_token
        self._time_entries = []

        self._get_time_entries()

    def _get_time_entries(self):
        since = self.start_date.strftime('%Y-%m-%d')
        until = self.end_date.strftime('%Y-%m-%d')
        request_url = TimeSheet.TOGGL_REPORT_URL.format(
            workspace_id=self.workspace.id,
            since=since, until=until,
            projects=self.project.id,
            rounding="true" if self.rounding else "false")

        resp = requests.get(request_url, auth=(self._api_token, 'api_token'))
        if resp.json() is not None:
            log.debug(resp.json())
            for entry in resp.json()['data']:
                time_entry = TimeEntry(**entry)
                self._time_entries.append(time_entry)

    @property
    def entries(self) -> List:
        return self._time_entries

    def export(self, path) -> bool:
        workbook = xlsxwriter.Workbook(path, {'remove_timezone': True})
        worksheet = workbook.add_worksheet(
            f"{self.start_date.strftime('%d-%m-%Y')} - {self.end_date.strftime('%d-%m-%Y')}")

        bold = workbook.add_format({'bold': 1})
        date_format = workbook.add_format({'num_format': 'dd-mm-yyyy'})
        time_format = workbook.add_format({'num_format': 'H:MM'})
        number_format = workbook.add_format({'num_format': '0.00'})

        columns = {
            'user': {'label': 'User', 'column': 'A'},
            'email': {'label': 'Email', 'column': 'B'},
            'client': {'label': 'Client', 'column': 'C'},
            'project': {'label': 'Project', 'column': 'D'},
            'task': {'label': 'Task', 'column': 'E'},
            'tags': {'label': 'Tags', 'column': 'F'},
            'description': {'label': 'Description', 'column': 'G'},
            'start_date': {'label': 'Start date', 'column': 'H'},
            'end_date': {'label': 'End date', 'column': 'I'},
            'duration_hours': {'label': 'Time (h)', 'column': 'J'},
            'duration_decimal': {'label': 'Time (decimal)', 'column': 'K'},
        }

        # write header
        for _, v in columns.items():
            worksheet.write(f"{v['column']}1", v['label'], bold)

        current_row = 2
        for entry in self.entries:
            tags = ""
            for t in entry.tags:
                tags += f" {t}"

            worksheet.write_string(f"{columns['user']['column']}{current_row}", entry.user)
            worksheet.write_string(f"{columns['email']['column']}{current_row}", self.email)
            worksheet.write_string(f"{columns['client']['column']}{current_row}", entry.client)
            worksheet.write_string(f"{columns['project']['column']}{current_row}", entry.project)
            worksheet.write_string(f"{columns['task']['column']}{current_row}", entry.task)
            worksheet.write_string(f"{columns['tags']['column']}{current_row}", tags)
            worksheet.write_string(f"{columns['description']['column']}{current_row}", entry.description)
            worksheet.write_datetime(f"{columns['start_date']['column']}{current_row}", entry.start, date_format)
            worksheet.write_datetime(f"{columns['end_date']['column']}{current_row}", entry.end, date_format)
            worksheet.write_datetime(f"{columns['duration_hours']['column']}{current_row}", entry.duration, time_format)
            worksheet.write_number(f"{columns['duration_decimal']['column']}{current_row}", entry.duration_decimal,
                                   number_format)
            current_row += 1

        worksheet.write(f"{columns['duration_decimal']['column']}{current_row}",
                        f"=SUM({columns['duration_decimal']['column']}2:{columns['duration_decimal']['column']}{current_row - 1})",
                        number_format, '')

        workbook.close()


def load_config(path: Path) -> Configuration:
    return Configuration(config_src=path.absolute().as_posix())


def main():
    usage = "Usage: %prog [options] path"
    parser = OptionParser(usage=usage, version=f"%prog {VERSION}")

    parser.add_option("-c", "--config", dest="config", help="Report config file", default=DEFAULT_CONFIG_PATH)
    parser.add_option("-a", "--api-token", dest="api_token", help="Toggl api token", metavar="TOKEN")
    parser.add_option("-w", "--workspace", dest="workspace", help="Toggl workspace containing the project")
    parser.add_option("-p", "--project", dest="project", help="Toggl project to generate the report for")
    parser.add_option("-e", "--email", dest="email", help="The e-mail address used in the report")
    parser.add_option("-s", "--since", dest="since", help="Report start date", metavar="DD-MM-YYYY")
    parser.add_option("-u", "--until", dest="until", help="Report end date", metavar="DD-MM-YYYY")
    parser.add_option("-f", "--format", dest="format", help="Output file format string", metavar="FORMAT")
    parser.add_option("-d", "--debug", action="store_true", help="Print debug information", default=False)
    parser.add_option("-q", "--quiet", action="store_true", help="Do not output status", default=False)

    (options, args) = parser.parse_args()

    if options.debug:
        log.setLevel(logging.DEBUG)
    if options.quiet:
        log.setLevel(logging.CRITICAL+1)

    # get dict format of options


    config_path = Path(options.config)
    config = {}
    if config_path.exists():
        # remove not used opts
        opts_config = {k: v for k, v in vars(options).items() if v is not None}

        try:
            conf = load_config(config_path)
            config['tag'] = conf.tag
        except FileNotFoundError as e:
            log.error(f"Could not load config file {config_path}: {e}")
            return 1

        for k in conf.config_keys:
            config |= conf.get_config(k)
    else:
        # keep all options
        opts_config = vars(options)

    if len(args) > 0:
        opts_config['path'] = Path(args[0])
    else:
        opts_config['path'] = Path('.')

    log.debug(f"file config: {config}")
    log.debug(f"options config: {opts_config}")

    config |= opts_config
    log.debug(f"new config: {config}")

    # check for mandatory items
    for k in ['email', 'api_token', 'workspace', 'project']:
        if k not in config or config[k] is None:
            log.error(f"Missing {k}")
            return 1

    if 'format' not in config or config['format'] is None:
        config['format'] = DEFAULT_OUTPUT_FORMAT

    if 'until' in config and config['until'] is not None:
        config['until'] = datetime.strptime(config['until'], DEFAULT_DATE_FORMAT)
    else:
        config['until'] = datetime.now()

    if 'since' in config and config['since'] is not None:
        config['since'] = datetime.strptime(config['since'], DEFAULT_DATE_FORMAT)
        if config['since'] > config['until']:
            log.error('Report window lies in the future (since > until)')
            return 1
    else:
        config['since'] = config['until'] - timedelta(days=7)

    config['path'] = config['path'] / config['format'].format(project=config['project'],
                                                              since=config['since'].strftime(DEFAULT_DATE_FORMAT),
                                                              until=config['until'].strftime(DEFAULT_DATE_FORMAT))

    log.info(f"Loaded configuration: {config['tag']}")
    log.info(f" - Workspace: {config['workspace']}")
    log.info(f" - Project: {config['project']}")
    log.info(f" - Output: {config['path']}")
    log.info(f" - Since: {config['since']}")
    log.info(f" - Until: {config['until']}")

    workspaces = Workspaces(config['api_token'])
    workspaces.get_workspaces()
    ws = workspaces.workspace_by_name(config['workspace'])
    project = ws.project_by_name(config['project'])

    timesheet = TimeSheet(api_token=config['api_token'], email=config['email'], workspace=ws, project=project,
                          start_date=config['since'], end_date=config['until'])
    timesheet.export(config['path'])


if __name__ == '__main__':
    sys.exit(main())


